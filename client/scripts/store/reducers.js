import { POPULATE, SELECT, SET_TRANSLATE_TO, TRANSLATE, SET_CENTERPOINT } from './actions'
import { feature } from '@turf/turf';

const initialState = {
  records: undefined,
  selected: undefined,
  centerpoint: undefined,
  translate_to: []
}

export default function generalReducer(state=initialState, action) {
  switch (action.type) {
    case POPULATE:
      return {
        ...state,
        records: action.payload
      }
    case SELECT:
      return {
        ...state,
        selected: action.payload 
      }
    case SET_TRANSLATE_TO:
      return {
        ...state,
        translate_to: action.payload
      }
    case TRANSLATE:
      const records = state.records.features.map(feature => {
        if (feature.properties.id == action.payload.record.id) {
          feature.geometry.coordinates = action.payload.coordinates.reverse()
        }
        return feature
      })

      return {
        selected: undefined,
        translate_to: [],
        records: { type: "FeatureCollection", features: records }
      }
    case SET_CENTERPOINT:
      return {
        ...state,
        centerpoint: action.payload
      }
    default:
      return state
  }
}