import React, { useCallback } from 'react'
import { useMappedState } from 'redux-react-hook'
import FileUpload from './FileUpload'
import RecordProperties from './RecordProperties'
import Translate from './Translate'


export default function Home() {
  const mapState = useCallback(state => ({ selected: state.selected }))
  const { selected } = useMappedState(mapState)
  const recordProperties = selected ? <RecordProperties /> : undefined
  const translate = selected ? <Translate /> : undefined
  return (
    <div className="container">
      <h2>Tools</h2>
      <FileUpload />
      {translate}
      {recordProperties}
    </div>
  )
}