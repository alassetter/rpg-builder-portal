import React from 'react'

const styleObject = {
  width: '1rem',
  height: '1rem'
}

export default function Spinner() {
  return (
    <div className="spinner-border" role="status" style={styleObject}>
      <span className="sr-only">Loading...</span>
    </div>
  )
}