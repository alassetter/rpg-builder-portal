from orator.migrations import Migration


class AddNewColumnsToSpecsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.table('specs') as table:
            table.integer('elevation_id').unsigned().nullable()
            table.integer('plan_id').unsigned().nullable()
            table.foreign('elevation_id').references('id').on('elevations')
            table.foreign('plan_id').references('id').on('plans')


    def down(self):
        """
        Revert the migrations.
        """
        with self.schema.table('specs') as table:
            table.drop_column('elevation_id')
            table.drop_column('plan_id')
