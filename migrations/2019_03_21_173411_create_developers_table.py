from orator.migrations import Migration


class CreateDevelopersTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('developers') as table:
            table.increments('id')
            table.timestamps()
            table.string('name')
            table.string('website').nullable()
            table.string('phone').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('developers')
