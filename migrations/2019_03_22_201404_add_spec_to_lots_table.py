from orator.migrations import Migration


class AddSpecToLotsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.table('lots') as table:
            table.integer('spec_id').unsigned().nullable()
            table.foreign('spec_id').references('id').on('specs')

    def down(self):
        """
        Revert the migrations.
        """
        with self.schema.table('lots') as table:
            table.drop_column('spec_id')
