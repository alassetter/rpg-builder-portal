from orator.migrations import Migration


class CreateElevationsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('elevations') as table:
            table.increments('id')
            table.timestamps()
            table.string('name')
            table.long_text('description').nullable()
            table.boolean('front_porch')
            table.integer('plan_id').unsigned().nullable()
            table.foreign('plan_id').references('id').on('plans').nullable()
            table.enum('status', ['approved', 'denied', 'pending']).default('pending')
            table.date('approved_on').nullable()
            table.date('denied_on').nullable()


    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('elevations')
