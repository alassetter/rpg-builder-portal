from orator.migrations import Migration


class CreatePlansTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('plans') as table:
            table.increments('id')
            table.timestamps()
            table.string('name')
            table.long_text('description').nullable()
            table.integer('stories')
            table.integer('size')
            table.integer('min_beds')
            table.integer('max_beds')
            table.integer('min_baths')
            table.integer('max_baths')
            table.integer('min_sqft')
            table.integer('max_sqft')
            table.integer('min_price')
            table.integer('max_price')
            table.integer('min_garage')
            table.integer('max_garage')
            table.boolean('front_porch')
            table.boolean('front_loaded')

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('plans')
