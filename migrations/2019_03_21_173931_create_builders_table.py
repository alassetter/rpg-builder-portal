import json
from orator.migrations import Migration

schedule = {
    "monday": None,
    "tuesday": None,
    "wednesday": None,
    "thursday": None,
    "friday": None,
    "sunday": None,
}

class CreateBuildersTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('builders') as table:
            table.increments('id')
            table.timestamps()
            table.json('schedule').default(json.dumps(schedule))
            table.string('name')
            table.string('website').nullable()
            table.long_text('description').nullable()
            table.string('address').nullable()
            table.string('second_address').nullable()
            table.string('city').nullable()
            table.string('state').nullable()
            table.string('zipcode').nullable()
            table.string('phone').nullable()
            table.string('fax').nullable()
            table.datetime('approved_at').nullable()
            table.integer('approved_by_id').unsigned().nullable()
            table.foreign('approved_by_id').references(
                'id').on('users')


    def down(self):
        """
        Revert the migrations.
        """
        with self.schema.table('builders') as table:
            table.drop_foreign('approved_by_id')
        self.schema.drop('builders')
