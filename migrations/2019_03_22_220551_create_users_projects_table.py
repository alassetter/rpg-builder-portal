from orator.migrations import Migration


class CreateUsersProjectsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('users_projects') as table:
            table.increments('id')
            table.timestamps()
            table.integer('user_id').unsigned()
            table.integer('project_id').unsigned()
            table.foreign('user_id').references('id').on('users')
            table.foreign('project_id').references('id').on('projects')

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('users_projects')
