from orator.migrations import Migration


class CreateRulesAble(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('rules') as table:
            table.increments('id')
            table.timestamps()
            table.enum('side', ['right', 'left', 'front', 'back'])
            table.enum('operation', [
                'more than',
                'more or equal than',
                'less than',
                'less or equal than',
                'equals',
                'percentage'
            ])
            table.integer('quantity')

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('rules')
