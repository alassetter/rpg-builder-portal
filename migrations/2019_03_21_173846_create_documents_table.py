from orator.migrations import Migration


class CreateDocumentsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('documents') as table:
            table.increments('id')
            table.timestamps()
            table.string('name')
            table.long_text('description').nullable()
            table.string('tags').nullable()
            table.integer('project_id').unsigned().nullable()
            table.integer('created_by_id').unsigned().nullable()
            table.integer('plan_id').unsigned().nullable()
            table.integer('elevation_id').unsigned().nullable()
            table.integer('spec_id').unsigned().nullable()
            table.foreign('plan_id').references('id').on('plans').nullable()
            table.foreign('spec_id').references('id').on('specs').nullable()
            table.foreign('elevation_id').references('id').on('elevations').nullable()
            table.foreign('created_by_id').references('id').on('users')
            table.foreign('project_id').references('id').on('projects').nullable()


    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('documents')
