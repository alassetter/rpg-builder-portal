from orator.migrations import Migration


class AddNewColumnsToElevationsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.table('elevations') as table:
            table.integer('style_id').unsigned().nullable()
            table.integer('project_id').unsigned().nullable()
            table.foreign('style_id').references('id').on('styles')
            table.foreign('project_id').references('id').on('projects')

    def down(self):
        """
        Revert the migrations.
        """
        with self.schema.table('elevations') as table:
            table.drop_column('style_id')
            table.drop_column('project_id')
