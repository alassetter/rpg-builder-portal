from orator.migrations import Migration


class CreateRealtorsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('realtors') as table:
            table.increments('id')
            table.timestamps()
            table.string('name')
            table.string('email').nullable()
            table.string('phone').nullable()
            table.string('agency').nullable()
            table.string('agency_phone').nullable()
            table.string('agency_website').nullable()
            table.string('agency_address').nullable()
            table.integer('created_by_id').unsigned().nullable()
            table.foreign('created_by_id').references('id').on('users')

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('realtors')
