from orator.migrations import Migration


class CreateBuyersTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('buyers') as table:
            table.increments('id')
            table.timestamps()
            table.string('first_name')
            table.string('last_name')
            table.string('email')
            table.string('phone')
            table.string('spouse_first_name').nullable()
            table.string('spouse_last_name').nullable()
            table.string('spouse_email').nullable()
            table.string('spouse_phone').nullable()
            table.string('address')
            table.string('employer_name').nullable()
            table.string('employer_phone').nullable()
            table.string('employer_zipcode').nullable()
            table.string('spouse_employer_name').nullable()
            table.string('spouse_employer_phone').nullable()
            table.string('spouse_employer_zipcode').nullable()
            table.integer('age').nullable()
            table.boolean('married').nullable()
            table.integer('number_of_children').nullable()
            table.integer('created_by_id').unsigned().nullable()
            table.foreign('created_by_id').references('id').on('users')
            table.json('children').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('buyers')
