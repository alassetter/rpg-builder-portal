from orator.migrations import Migration


class CreateStylesTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('styles') as table:
            table.increments('id')
            table.timestamps()
            table.string('name')
            table.integer('project_id').unsigned().nullable()
            table.foreign('project_id').references('id').on('projects').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('styles')
