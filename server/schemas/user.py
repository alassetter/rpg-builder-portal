from marshmallow import fields, Schema


class UserSchema(Schema):
    email = fields.Email()
    second_email = fields.Email()
    first_name = fields.Str()
    last_name = fields.Str()
    phone = fields.Str()
    title = fields.Str()
    active_project_id = fields.Int()
    verified = fields.Bool()
    verified_at = fields.DateTime()
    schedule = fields.Dict()
    settings = fields.Dict()
    role = fields.Str()
