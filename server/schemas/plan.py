from marshmallow import fields, Schema


class PlanSchema(Schema):
    name = fields.Str()
    description = fields.Str()
    stories = fields.Int()
    size = fields.Int()
    min_beds = fields.Int()
    max_beds = fields.Int()
    min_baths = fields.Int()
    max_baths = fields.Int()
    min_sqft = fields.Int()
    max_sqft = fields.Int()
    min_price = fields.Int()
    max_price = fields.Int()
    min_garage = fields.Int()
    max_garage = fields.Int()
    front_porch = fields.Bool()
    front_loaded = fields.Bool()
    project = fields.Nested('ProjectSchema')
