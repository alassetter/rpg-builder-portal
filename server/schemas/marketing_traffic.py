from marshmallow import fields, Schema


class MarketingTrafficSchema(Schema):
    weekday_visitors = fields.Int()
    weekend_visitors = fields.Int()
    start_on = fields.Date()
    end_on = fields.Date()
    project_id = fields.Int()
