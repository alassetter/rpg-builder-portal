from marshmallow import fields, Schema


class RealtorSchema(Schema):
    name = fields.Str()
    email = fields.Email()
    phone = fields.Str()
    agency = fields.Str()
    agency_phone = fields.Str()
    agency_website = fields.Str()
    agency_address = fields.Str()
