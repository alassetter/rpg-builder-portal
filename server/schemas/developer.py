from marshmallow import fields, Schema


class DeveloperSchema(Schema):
    name = fields.Str()
    website = fields.Str()
    phone = fields.Str()