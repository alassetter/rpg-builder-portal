from marshmallow import fields, Schema


class ProjectSchema(Schema):
    name = fields.Str()
    description = fields.Str()
    config = fields.Dict()