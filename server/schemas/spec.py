from marshmallow import fields, Schema
from ..schemas import ElevationSchema, PlanSchema


class SpecSchema(Schema):
    price = fields.Int()
    sqft = fields.Int()
    front_porch = fields.Bool()
    description = fields.Str()
    available_on = fields.Date()
    is_arc = fields.Bool()
    elevation = fields.Nested('ElevationSchema')
    plan = fields.Nested('PlanSchema')
    approved_on = fields.Date()
    denied_on = fields.Date()
    project = fields.Nested('ProjectSchema')
