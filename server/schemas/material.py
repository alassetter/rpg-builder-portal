from marshmallow import fields, Schema


class MaterialSchema(Schema):
    name = fields.Str()
    category = fields.Str()
    manufacturer = fields.Str()
    status = fields.Str()
    type = fields.Str()