from marshmallow import fields, Schema
from .project import ProjectSchema
from .buyer import BuyerSchema
from .realtor import RealtorSchema


class SaleSchema(Schema):
    price = fields.Int()
    sqft = fields.Int()
    project = fields.Nested(ProjectSchema)
    buyer = fields.Nested(BuyerSchema)
    realtor = fields.Nested(RealtorSchema)
    is_closed = fields.Bool()
    is_cancelled = fields.Bool()
    sold_on = fields.Date()
    closed_on = fields.Date()
    estimated_close_on = fields.Date()
    cancelled_on = fields.Date()
    project = fields.Nested('ProjectSchema')
