from marshmallow import fields, Schema


class BuyerSchema(Schema):
    id = fields.Int()
    first_name = fields.Str()
    last_name = fields.Str()
    email = fields.Email()
    phone = fields.Str()
    spouse_first_name = fields.Str()
    spouse_last_name = fields.Str()
    spouse_email = fields.Email()
    spouse_phone = fields.Str()
    address = fields.Str()
    employer_name = fields.Str()
    employer_phone = fields.Str()
    employer_zipcode = fields.Str()
    spouse_employer_name = fields.Str()
    spouse_employer_phone = fields.Str()
    spouse_employer_zipcode = fields.Str()
    age = fields.Int()
    married = fields.Boolean()
    number_of_children = fields.Int()
    children = fields.List(fields.Dict())
