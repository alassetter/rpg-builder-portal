import os, calendar
from flask import Flask, render_template
from flask_webpack import Webpack
from flask_apispec import FlaskApiSpec
from flask_orator import Orator
from orator.orm import Factory
from flask_restful import Api
from .config import config

api = Api()
db = Orator()
factory = Factory()
docs = FlaskApiSpec()
webpack = Webpack()

def create_app():
    app = Flask(__name__)
    environment = os.environ['FLASK_ENV']
    config_object = config[environment]

    if environment == 'production':
        config_object['ORATOR_DATABASES'] = {
            'default': 'production',
            'production': {
                'driver': 'mysql',
                'host': os.environ.get('DATABASE_HOST'),
                'database': os.environ.get('DATABASE_NAME'),
                'user': os.environ.get('DATABASE_USER'),
                'password': os.environ.get('DATABASE_PASSWORD')
            }
        }

    app.config.from_object(config_object)
    app.static_url_path = app.config.get('STATIC_FOLDER')
    app.static_folder = app.root_path.replace('/server', '') + app.static_url_path
    config_object.init_app(app)
    db.init_app(app)
    configure_resources(api, docs)
    api.init_app(app)
    docs.init_app(app)
    webpack.init_app(app)

    @app.route('/')
    def index():
        return render_template('index.jinja2')
    return app


def configure_resources(api, docs):
    from .resources import (
        LotResource,
        LotsResource,
        BuilderResource,
        BuildersResource,
        ElevationResource,
        ElevationsResource,
        MarketingTrafficResource,
        MarketingTrafficsResource,
        SaleResource,
        SalesResource,
        PlanResource,
        PlansResource,
        SpecResource,
        SpecsResource,
        MaterialResource,
        MaterialsResource,
        ProjectResource,
        ProjectsResource,
        DXFResource
    )

    # Lots
    api.add_resource(LotResource, '/lots/<lot_id>')
    docs.register(LotResource)
    api.add_resource(LotsResource, '/lots')
    docs.register(LotsResource)

    # Builders
    api.add_resource(BuilderResource, '/builders/<builder_id>')
    docs.register(BuilderResource)
    api.add_resource(BuildersResource, '/builders')
    docs.register(BuildersResource)

    # Elevation
    api.add_resource(ElevationResource, '/elevations/<elevation_id>')
    docs.register(ElevationResource)
    api.add_resource(ElevationsResource, '/elevations')
    docs.register(ElevationsResource)

    # Marketing Traffic
    api.add_resource(MarketingTrafficResource,
                     '/reports/marketing/<marketing_traffic_id>')
    docs.register(MarketingTrafficResource)
    api.add_resource(MarketingTrafficsResource, '/reports/marketing')
    docs.register(MarketingTrafficsResource)

    # Sales
    api.add_resource(SaleResource, '/sales/<sale_id>')
    docs.register(SaleResource)
    api.add_resource(SalesResource, '/sales')
    docs.register(SalesResource)

    # Plan
    api.add_resource(PlanResource, '/plans/<plan_id>')
    docs.register(PlanResource)
    api.add_resource(PlansResource, '/plans')
    docs.register(PlansResource)

    # Spec
    api.add_resource(SpecResource, '/specs/<spec_id>')
    docs.register(SpecResource)
    api.add_resource(SpecsResource, '/specs')
    docs.register(SpecsResource)

    # Material
    api.add_resource(MaterialResource, '/materials/<material_id>')
    docs.register(MaterialResource)
    api.add_resource(MaterialsResource, '/materials')
    docs.register(MaterialsResource)

    # Project
    api.add_resource(ProjectResource, '/projects/<project_id>')
    docs.register(ProjectResource)
    api.add_resource(ProjectsResource, '/projects')
    docs.register(ProjectsResource)

    # DXF
    api.add_resource(DXFResource, '/dxf')
    docs.register(DXFResource)
