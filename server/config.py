import calendar
from flask_dotenv import DotEnv
from flask.json import JSONEncoder
from datetime import datetime, date
from dateutil.parser import parse

# class CustomJsonEncoder(JSONEncoder):
#     def default(self, obj):
#         try:
#             if isinstance(obj, datetime) or isinstance(obj, date):
#                 return str(obj)
#             iterable = iter(obj)
#         except TypeError:
#             pass
#         else:
#             return list(iterable)

#         return JSONEncoder.default(self, obj)


class Config(object):
    APISPEC_SWAGGER_URL = '/docs/json'
    APISPEC_SWAGGER_UI_URL = '/docs'
    # RESTFUL_JSON = {'cls': CustomJsonEncoder}
    WEBPACK_MANIFEST_PATH = '../static/manifest.json'
    STATIC_FOLDER = '/static'

    @classmethod
    def init_app(self, app):
        # app.config['RESTFUL_JSON']['cls'] = app.json_encoder = CustomJsonEncoder
        env = DotEnv()
        env.init_app(app)

class DevelopmentConfig(Config):
    DEBUG = True
    TESTING = False
    ORATOR_DATABASES = {
        'default': 'development',
        'development': {
            'driver': 'sqlite',
            'database': './database.db',
            'foreign_keys': False
        }
    }

class ProductionConfig(Config):
    DEBUG = False
    TESTING = False

class TestConfig(Config):
    DEBUG = True
    TESTING = True
    ORATOR_DATABASES = {
        'default': 'test',
        'test': {
            'driver': 'sqlite',
            'database': ':memory:',
            'foreign_keys': False
        }
    }

config = {
    'development': DevelopmentConfig,
    'test': TestConfig,
    'production': ProductionConfig
}
