class AntimonotonyFilter(object):
    def __init__(self, resource, lot):
        self.resource = resource
        self.lot = lot

    @property
    def resource_name(self):
        return self.resource.__name__.lower()

    def filter(self):
        from ..models import Rule
        filtered_resources = []
        for rule in Rule.exists("%s_id" % self.resource.__name__.lower()):
            neighbours = self.neighbours[rule.side]
            for index, neighbor in enumerate(neighbours, 1):
                resources = self.resources(neighbours)
                if rule.operation == '>':
                    if index > rule.quantity:
                        filtered_resources = filtered_resources + resources
                elif rule.operation == '>=':
                    if index >= rule.quantity:
                        filtered_resources = filtered_resources + resources
                elif rule.operation == '<':
                    if index < rule.quantity:
                        filtered_resources = filtered_resources + resources
                elif rule.operation == '<=':
                    if index <= rule.quantity:
                        filtered_resources = filtered_resources + resources
                elif rule.operation == '=':
                    if index == rule.quantity:
                        filtered_resources = filtered_resources + resources

        filtered_resources_ids = set(map(
            lambda resource: resource.id, reduce(list.__add__, filtered_resources) if not len(filtered_resources) == 0 else []))
        return self.resource.exclude('id', filtered_resources_ids)

    @property
    def neighbours(self):
        return self.lot.neighbours.data

    def resources(self, neighbours):
        resources = {}
        for neighbor in neighbours:
            resources[neighbor.id] = getattr(
                neighbor, "%ss" % self.resource_name).all()
        return resources.values()
