import json
from unittest import TestCase
from os.path import exists as file_exists
from os import remove as file_remove
from os.path import join as path_join
from ..lib import DxfConverter, ShapefileImporter


class TestDxfConverter(TestCase):
    def test_convert_importer(self):
        path = '/tmp'
        converter = DxfConverter('./files/features/DXF/LOTS.dxf')
        converter.store(path)
        self.assertIsNone(converter.shp)
        self.assertIsNone(converter.shx)
        self.assertIsNone(converter.dbf)
        importer = ShapefileImporter('/tmp/LOTS.shp')
        self.assertTrue(file_exists(path + '/LOTS.shp'))
        self.assertTrue(file_exists(path + '/LOTS.shx'))
        self.assertTrue(file_exists(path + '/LOTS.dbf'))
        with open('/tmp/lots.geojson', 'w+') as f:
            f.write(json.dumps(importer.data))
        file_remove(path_join(path, 'LOTS.shp'))
        file_remove(path_join(path, 'LOTS.shx'))
        file_remove(path_join(path, 'LOTS.dbf'))
