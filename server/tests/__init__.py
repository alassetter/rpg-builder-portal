from unittest import TestCase
from flask import current_app
import json


class TestCaseMixin(TestCase):
    @classmethod
    def setUpClass(cls):
        from .. import db, create_app
        from orator.migrations import Migrator, DatabaseMigrationRepository
        from os import environ, getcwd
        from os.path import join as path_join
        environ.setdefault('FLASK_ENV', 'test')

        try:
            assert current_app.config
            cls.app = current_app
        except RuntimeError:
            cls.app = create_app()

        if cls is not TestCaseMixin and cls.setUp is not TestCaseMixin.setUp:
            original_setUp = cls.setUp

            def setUpOverride(self, *args, **kwargs):
                repository = DatabaseMigrationRepository(
                    db, 'migrations')
                migrator = Migrator(repository, db)
                if not migrator.repository_exists():
                    repository.create_repository()
                migrations_path = path_join(getcwd(), 'migrations')
                migrator.run(migrations_path)
                TestCaseMixin.migrations_path = migrations_path
                TestCaseMixin.migrator = migrator
                app_context = cls.app.app_context()
                app_context.push()
                self.app = current_app
                TestCaseMixin.setUp(self)
                return original_setUp(self, *args, **kwargs)

            cls.setUp = setUpOverride

    @classmethod
    def tearDownClass(cls):
        if cls is not TestCaseMixin and cls.tearDown is not TestCaseMixin.tearDown:
            original_tearDown = cls.tearDown

            def tearDownOverride(self, *args, **kwargs):
                if TestCaseMixin.migrator.repository_exists():
                    TestCaseMixin.migrator.rollback(
                        TestCaseMixin.migrations_path)
                return original_tearDown(self, *args, **kwargs)
            cls.tearDown = tearDownOverride

    def setUp(self):
        from .. import factory
        self.client = self.app.test_client()
        self.factory = factory
        self.json = json

    def tearDown(self):
        pass
