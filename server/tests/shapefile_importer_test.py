import json
from unittest import TestCase
from ..lib import ShapefileImporter


class TestShapefileImporter(TestCase):
    def setUp(self):
        self.importer = ShapefileImporter('./files/lots.shp')

    def test_data(self):
        self.assertTrue(len(self.importer.data) > 0)

    def test_lots_import(self):
        importer = ShapefileImporter('./files/features/SHP/LOTS.shp')
        features = self.importer.data
        with open('/tmp/lots.geojson', 'w+') as f:
            f.write(json.dumps(self.importer.data))
