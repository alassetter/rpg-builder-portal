import six
from flask_apispec.views import MethodResourceMeta
from flask_restful import Resource


class ResourceMixin(six.with_metaclass(MethodResourceMeta, Resource)):
    pass
