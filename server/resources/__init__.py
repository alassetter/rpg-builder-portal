from .lot import LotResource, LotsResource
from .builder import BuilderResource, BuildersResource
#   from .buyer import BuyerResource, BuyersResource
#   from .developer import DeveloperResource, DevelopersResource
from .elevation import ElevationResource, ElevationsResource
#   from .marketing_traffic import MarketingTrafficResource, MarketingTrafficsResources
from .marketing_traffic import MarketingTrafficResource, MarketingTrafficsResource
from .sale import SaleResource, SalesResource
from .plan import PlanResource, PlansResource
from .spec import SpecResource, SpecsResource
from .material import MaterialResource, MaterialsResource
from .project import ProjectResource, ProjectsResource
from .dxf import DXFResource